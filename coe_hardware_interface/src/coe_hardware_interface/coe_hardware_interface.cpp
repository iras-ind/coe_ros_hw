#include <pluginlib/class_list_macros.h>

#include <coe_hardware_interface/coe_hardware_interface.h>
#include <coe_core/ds402/coe_xfsm_symbols.h>

PLUGINLIB_EXPORT_CLASS(cnr_hardware_interface::CoeRobotHW, cnr_hardware_interface::RobotHW)

namespace cnr_hardware_interface
{
void setParam(CoeRobotHW* hw, const std::string& ns)
{
  std::vector<double> pos, vel, eff;
  hw->getJointState(pos, vel, eff);

  std::vector<double> tpos, tvel, teff;
  hw->getJointCommand(tpos, tvel, teff);

  if(pos.size() > 0)
  {
    hw->m_robothw_nh.setParam("status/" + ns + "/feedback/position", pos);
    hw->m_robothw_nh.setParam("status/" + ns + "/feedback/velocity", vel);
    hw->m_robothw_nh.setParam("status/" + ns + "/feedback/effort"  , eff);
    hw->m_robothw_nh.setParam("status/" + ns + "/command/position" , tpos);
    hw->m_robothw_nh.setParam("status/" + ns + "/command/velocity" , tvel);
    hw->m_robothw_nh.setParam("status/" + ns + "/command/effort"   , teff);
  }
}


CoeRobotHW::CoeRobotHW()
{
  write_time_ = -1.0;
  read_time_ = -1.0;
  max_read_error_ = 0;
  
  //m_devices=devices;
  m_set_status_param = boost::bind(setParam,this,_1);
}

bool CoeRobotHW::doInit()
{
  CNR_TRACE_START(this->m_logger);

// const std::map<std::string,std::pair<std::string,int>>& devices
  XmlRpc::XmlRpcValue config;
  if(!m_robothw_nh.getParam("coe_ros_plugins", config))
  {
    CNR_FATAL(this->m_logger, m_robothw_nh.getNamespace()<<"/coe_ros_plugins' does not exist");
    CNR_RETURN_FALSE(this->m_logger);
  }
  if(config.getType() != XmlRpc::XmlRpcValue::TypeArray)
  {
    CNR_FATAL(this->m_logger, m_robothw_nh.getNamespace()<<"/coe_ros_plugins' is not a list of devices");
    CNR_RETURN_FALSE(this->m_logger);
  }
  CNR_WARN(m_logger, "Scan of feasible coe_ros_plugins list(number of plugin:" << config.size() << ")" );
  for(auto i=0; i < config.size(); i++)
  {
    XmlRpc::XmlRpcValue cfg = config[i];
    if(cfg.getType() != XmlRpc::XmlRpcValue::TypeStruct)
    {
      CNR_WARN(m_logger, "The element #" << i << " is not a struct");
      continue;
    }
    if(!cfg.hasMember("name"))
    {
      CNR_WARN(m_logger, "The element #" << i << " has not the field 'name'");
      continue;
    }
    if(!cfg.hasMember("type"))
    {
      CNR_WARN(m_logger, "The element #" << i << " has not the field 'type'");
      continue;
    }
    if(!cfg.hasMember("address"))
    {
      CNR_WARN(m_logger, "The element #" << i << " has not the field 'address'");
      continue;
    }

    CNR_DEBUG(m_logger, "Add device " <<((std::string)cfg["name"]) <<", type " <<
                 ((std::string)cfg["type"]) << ", address " <<((int)cfg["address"]));
    std::pair<std::string,std::pair<std::string,int>> new_device((std::string)cfg["name"],
                                        std::pair<std::string,int>((std::string)cfg["type"],(int)cfg["address"]));
    m_devices.insert(new_device);
  }

  std::string soem_ns;
  if(!this->m_robothw_nh.getParam("soem_ns",soem_ns))
  {
    CNR_ERROR(m_logger, "Failed in extracting the param '"<< m_robothw_nh.getNamespace() <<"/soem_ns' is not set");
    CNR_RETURN_FALSE(m_logger);
  }
  boost::shared_ptr<coe_master::CoeHwPlugin> device_instance;
  boost::shared_ptr<pluginlib::ClassLoader<coe_master::CoeHwPlugin>> planner_plugin_loader;

  m_pos .resize(m_devices.size());
  m_vel .resize(m_devices.size());
  m_eff .resize(m_devices.size());
  m_tpos.resize(m_devices.size());
  m_tvel.resize(m_devices.size());
  m_teff.resize(m_devices.size());

  unsigned int iDev=0;
  for(const std::pair<std::string,std::pair<std::string,int>>& device: m_devices)
  {

    int address=(device.second).second;
    std::string device_type=(device.second).first;
    std::string name=device.first;

    CNR_DEBUG(m_logger,"device_name="<<device.first<<", adress address="<< address);

    try
    {
      planner_plugin_loader.reset(new
          pluginlib::ClassLoader<coe_master::CoeHwPlugin>("coe_master", "coe_master::CoeHwPlugin"));
    }
    catch(pluginlib::PluginlibException& ex)
    {
      CNR_FATAL(m_logger, "Exception while creating device plugin loader " << ex.what());
      continue;
    }


    try
    {
      CNR_DEBUG(m_logger,"Loading instance '"<<name<<"' of type: '"<<device_type<<"'");
      planner_plugin_loader->loadLibraryForClass(device_type);
      CNR_DEBUG(m_logger,"Is Class Available? " <<(planner_plugin_loader->isClassAvailable(device_type)?"YES":"NO"));
      CNR_DEBUG(m_logger,"Name of the class? "<< planner_plugin_loader->getName(device_type));

      CNR_DEBUG(m_logger,"Create Instance '" << name <<"' of type: '"<<device_type<<"'");

      device_instance = planner_plugin_loader->createInstance(device_type);

      if(!device_instance->initialize(m_root_nh,soem_ns,address))
      {
        CNR_ERROR(m_logger, "Could not initialize device instance: " << name
                        << ", address: " << address 
                        << ", root ns: " << this->m_root_nh.getNamespace()
                        << ", soem ns: " << soem_ns);
        CNR_RETURN_FALSE(this->m_logger);
      }
    }
    catch(pluginlib::PluginlibException& ex)
    {
      const std::vector<std::string>& classes = planner_plugin_loader->getDeclaredClasses();
      std::stringstream ss;
      for(std::size_t i = 0; i < classes.size(); ++i)
        ss << classes[i] << " ";
      CNR_ERROR(m_logger, "Exception while loading planner '" << name << "': " << ex.what() << "\n"
                            << "Available plugins: " << ss.str());
      CNR_RETURN_FALSE(this->m_logger);
    }

    if(device_instance->isActuator())
    {
      CNR_DEBUG(m_logger,"Device '"<<name<<"'(type: '"<<device_type<< "') is an actuator DS402");
      device_instance->jointStateHandle(&m_pos.at(iDev), &m_vel.at(iDev), &m_eff.at(iDev));

      hardware_interface::JointStateHandle js_handle(name, m_pos.at(iDev), m_vel.at(iDev), m_eff.at(iDev));
      m_js_jh.registerHandle(js_handle);

      device_instance->jointCommandHandle(&m_tpos.at(iDev), &m_tvel.at(iDev), &m_teff.at(iDev));

      std::vector<std::string> available_modes= device_instance->getStateNames();

      bool can_use_p, can_use_v, can_use_e, can_use_pve, can_use_ve;
      can_use_p=can_use_v=can_use_e=can_use_pve=can_use_ve=false;

      for(const std::string& mode: available_modes)
      {
        coe_core::ds402::ModeOperationID id=coe_core::ds402::to_modeofoperationid(mode);
        switch(id)
        {
//           case coe_core::ds402::MOO_PROFILED_POSITION:
//           case coe_core::ds402::MOO_INTERPOLATED_POSITION:
//           case coe_core::ds402::MOO_HOMING:
          case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION:
            can_use_p=true;
            can_use_pve=true;
            break;
//           case coe_core::ds402::MOO_PROFILED_VELOCITY:
//           case coe_core::ds402::MOO_OPEN_LOOP_VELOCITY:
          case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY:
            can_use_v=true;
            break;
//           case coe_core::ds402::MOO_PROFILED_TORQUE:
          case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_TORQUE:
            can_use_e=true;
            break;
          default:
            break;
        }
      }
      if(can_use_p)
      {
        hardware_interface::JointHandle pos_handle(js_handle,m_tpos.at(iDev));
        m_p_jh.registerHandle(pos_handle);
      }
      if(can_use_v)
      {
        hardware_interface::JointHandle vel_handle(js_handle,m_tvel.at(iDev));
        m_v_jh.registerHandle(vel_handle);
      }
      if(can_use_e)
      {
        hardware_interface::JointHandle eff_handle(js_handle,m_teff.at(iDev));
        m_e_jh.registerHandle(eff_handle);
      }
      if(can_use_pve)
      {
        *m_tvel.at(iDev) = *m_teff.at(iDev) = 0.0;
        hardware_interface::PosVelEffJointHandle pve_handle(js_handle,m_tpos.at(iDev),m_tvel.at(iDev),m_teff.at(iDev));
        m_pve_jh.registerHandle(pve_handle);
      }
      if(can_use_ve)
      {
        hardware_interface::VelEffJointHandle ve_handle(js_handle,m_tvel.at(iDev), m_teff.at(iDev));
        m_ve_jh.registerHandle(ve_handle);
      }
      m_joint_to_device_map.insert(std::make_pair(name,device_instance));
    }


    if(device_instance->hasAnalogInputs())
    {
      std::vector<std::string> analog_inputs=device_instance->getAnalogInputNames();
      m_ai.resize(analog_inputs.size());
      for(std::size_t idx=0;idx<analog_inputs.size();idx++)
      {
        m_ai.at(idx) = device_instance->analogInputValueHandle(analog_inputs.at(idx));
        hardware_interface::AnalogStateHandle handle(analog_inputs.at(idx),m_ai.at(idx));
        m_as_jh.registerHandle(handle);
      }
    }

    if(device_instance->hasAnalogOutputs())
    {
      std::vector<std::string> analog_outputs=device_instance->getAnalogOutputNames();
      m_ao.resize(analog_outputs.size());
      for(std::size_t idx=0;idx<analog_outputs.size();idx++)
      {
        m_ao.at(idx) = device_instance->analogOutputValueHandle(analog_outputs.at(idx));
        hardware_interface::AnalogStateHandle handle(analog_outputs.at(idx),m_ao.at(idx));
        hardware_interface::AnalogHandle command_handle(handle,m_ao.at(idx));
        m_as_jh.registerHandle(handle);
        m_ao_jh.registerHandle(command_handle);
      }
    }

    if(device_instance->hasDigitalInputs())
    {
      std::vector<std::string> digital_inputs=device_instance->getDigitalInputNames(); 
      m_di.resize(digital_inputs.size());
      for(std::size_t idx=0;idx<digital_inputs.size();idx++)
      {
        m_di.at(idx) = device_instance->digitalInputValueHandle(digital_inputs.at(idx));
        hardware_interface::DigitalStateHandle handle(digital_inputs.at(idx),m_di.at(idx));
        m_ds_jh.registerHandle(handle);
      }
    }

    if(device_instance->hasDigitalOutputs())
    {
      std::vector<std::string> digital_outputs=device_instance->getDigitalOutputNames();
      m_do.resize(digital_outputs.size());
      for(std::size_t idx=0;idx<digital_outputs.size();idx++)
      {
        m_do.at(idx) = device_instance->digitalOutputValueHandle(digital_outputs.at(idx));
        hardware_interface::DigitalStateHandle handle(digital_outputs.at(idx),m_do.at(idx));
        hardware_interface::DigitalHandle command_handle(handle,m_do.at(idx));
        m_ds_jh.registerHandle(handle);
        m_do_jh.registerHandle(command_handle);
      }
    }

    m_devices_types_map.insert   (std::make_pair(name, device_type));
    m_devices_map.insert         (std::make_pair(name, device_instance));
    m_planner_plugin_loader.insert(std::make_pair(name, planner_plugin_loader));

    CNR_DEBUG(m_logger,"Device '"<<device_type<<"'(type: '"<< name <<"') is an actuator DS402 OK");
    iDev++;
  }

  registerInterface(&m_js_jh);
  registerInterface(&m_p_jh);
  registerInterface(&m_v_jh);
  registerInterface(&m_e_jh);
  registerInterface(&m_ve_jh);
  registerInterface(&m_pve_jh);
  registerInterface(&m_as_jh);
  registerInterface(&m_ao_jh);
  registerInterface(&m_ds_jh);
  registerInterface(&m_do_jh);
  CNR_INFO(this->m_logger, "[" << BOLDWHITE()<<" OK " << RESET() << GREEN() << "] Initialize the CoeRobotHw");
  CNR_RETURN_TRUE(this->m_logger);
}

bool CoeRobotHW::doPrepareSwitch(const std::list< hardware_interface::ControllerInfo >& start_list,
                                  const std::list< hardware_interface::ControllerInfo >& /*stop_list*/)
{
  CNR_TRACE_START(m_logger);
  if(start_list.size()==0)
  {
    CNR_DEBUG(m_logger,"No new controllers to start");
  }

  for(const hardware_interface::ControllerInfo& controller: start_list)
  {
    for(const hardware_interface::InterfaceResources& res: controller.claimed_resources)
    {
      CNR_DEBUG(m_logger,"controller name: '"<<controller.name<<"' type: '"
                  << controller.type<<"' hi: '" <<res.hardware_interface<<"'");

      coe_core::ds402::ModeOperationID new_id;
      std::string new_id_string;

      if(!controller.type.compare("cnr/control/HomingController")
       ||!controller.type.compare("itia/control/HomingController"))
      {
        new_id=coe_core::ds402::MOO_HOMING;
        new_id_string="MOO_HOMING";
      }
      else if(res.hardware_interface=="hardware_interface::PosVelEffJointInterface")
      {
        new_id=coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION;
        new_id_string="MOO_CYCLIC_SYNCHRONOUS_POSITION";
      }
      else if(res.hardware_interface=="hardware_interface::PositionJointInterface")
      {
        new_id=coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION;
        new_id_string="MOO_CYCLIC_SYNCHRONOUS_POSITION";
      }
      else if(res.hardware_interface=="hardware_interface::VelocityJointInterface")
      {
        new_id=coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY;
        new_id_string="MOO_CYCLIC_SYNCHRONOUS_VELOCITY";
      }
      else if(res.hardware_interface=="hardware_interface::EffortJointInterface")
      {
        new_id=coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_TORQUE;
        new_id_string="MOO_CYCLIC_SYNCHRONOUS_TORQUE";
      }
      else
      {
        continue;
      }
      CNR_DEBUG(m_logger,"new mode: "<<new_id_string);
      CNR_DEBUG(m_logger,"resources size: " << res.resources.size());
      for(const std::string& joint: res.resources)
        m_joint_to_device_map.at(joint)->setSoftRT();
      
      for(const std::string& joint: res.resources)
      {
        CNR_DEBUG(m_logger,"  joint " << joint);

        // check if request mode is available
        std::vector<std::string> available_modes= m_joint_to_device_map.at(joint)->getStateNames();
        bool is_present=false;
        for(const std::string& mode: available_modes)
        {
          coe_core::ds402::ModeOperationID id=coe_core::ds402::to_modeofoperationid(mode);
          if(id==new_id)
          {
            is_present=true;
            break;
          }
        }
        if(!is_present)
        {
          CNR_ERROR(m_logger, "Controller " << controller.name << "can not be run of joint " << joint);
          CNR_RETURN_FALSE(m_logger);
        }

        coe_core::ds402::ModeOperationID act_id=coe_core::ds402::to_modeofoperationid(m_joint_to_device_map.at(joint)->getActualState());
        if(act_id==new_id)
        {
          CNR_WARN(m_logger, "Controller '"<< controller.name << "' is already in mode '" << new_id_string <<"'");
        }

        try
        {
          if(!m_joint_to_device_map.at(joint)->setTargetState(new_id_string, true))
          {
            CNR_ERROR(m_logger,"Impossible to switch joint '" << joint <<"' to state '"<<new_id_string<<"'");
            CNR_RETURN_FALSE(m_logger);
          }
          CNR_DEBUG(m_logger,"Controller " << controller.name << "is switched in mode " << new_id_string);
        }
        catch(std::exception& e)
        {
          CNR_ERROR(m_logger,"Impossible to switch joint '" << joint <<"' to state '"<<new_id_string<<"'");
          CNR_ERROR(m_logger,"Exception: " << e.what());
          CNR_RETURN_FALSE(m_logger);
        }
      }

      for(const std::string& joint: res.resources)
        m_joint_to_device_map.at(joint)->setHardRT();
    }
  }
  CNR_RETURN_TRUE(m_logger);
}

void CoeRobotHW::getJointState(std::vector<double>& pos, std::vector<double>& vel, std::vector<double>& eff)
{
  pos.clear(); vel.clear(); eff.clear();
  for(auto& p: m_devices_map)
  {
    if(p.second->isActuator())
    {
      std::vector<double> _pos, _vel, _eff;
      p.second->getJointState(_pos, _vel, _eff);
      pos.insert(pos.end(), _pos.begin(), _pos.end());
      vel.insert(vel.end(), _vel.begin(), _vel.end());
      eff.insert(eff.end(), _eff.begin(), _eff.end());
    }
  }
}

void CoeRobotHW::getJointCommand(std::vector<double>& pos, std::vector<double>& vel, std::vector<double>& eff)
{
  pos.clear(); vel.clear(); eff.clear();
  for(auto& p: m_devices_map)
  {
    if(p.second->isActuator())
    {
      std::vector<double> _pos, _vel, _eff;
      p.second->getJointCommand(_pos, _vel, _eff);
      pos.insert(pos.end(), _pos.begin(), _pos.end());
      vel.insert(vel.end(), _vel.begin(), _vel.end());
      eff.insert(eff.end(), _eff.begin(), _eff.end());
    }
  }
}

bool CoeRobotHW::doRead(const ros::Time& /*time*/, const ros::Duration& /*period*/)
{
  CNR_TRACE_START_THROTTLE_DEFAULT(this->m_logger);

  double prev_read_time_ = read_time_;
  
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  read_time_ =(double)(ts.tv_sec) +((double) ts.tv_nsec)/1.e9;
  if((prev_read_time_ > 0) &&(read_time_ - prev_read_time_ > 0.010))
  {
    CNR_WARN_THROTTLE(m_logger, 5.0, "The plugin reading latency is larger than 10ms, is it correct?");
  }

  std::stringstream report;
  for(auto& p: m_devices_map)
  {
    switch(p.second->read())
    {
      case coe_master::CoeHwPlugin::NONE_ERROR:
        break;
      case coe_master::CoeHwPlugin::COM_ERROR:
        report << "READ from shmem failed. Error in communication. Abort.";
        this->addDiagnosticsMessage("ERROR", "READ", { {"READ", "COM ERROR"} }, &report);
        m_status = cnr_hardware_interface::StatusHw::ERROR;
        CNR_ERROR_THROTTLE(m_logger, 5.0, "[ "<<p.second->getUniqueId()<<" ] READ - "<< report.str());
      break;
      case coe_master::CoeHwPlugin::DEVICE_ERROR:
        report << "The driver is in FAULT(or better, not in OPERATION ENABLED). Try to force reset.";
        this->addDiagnosticsMessage("ERROR", "READ", { {"READ", "The driver is in FAULT(or better, not in OPERATION ENABLED). Try to force reset."} }, &report);
        CNR_ERROR_THROTTLE(m_logger, 5.0, "[ "<<p.second->getUniqueId()<<" ] READ - "<< report.str());
        m_status = cnr_hardware_interface::StatusHw::ERROR;
        break;
      case coe_master::CoeHwPlugin::EXCEPTION_ERROR:
        report << "READ - Unmanaged Exception! driver is in FAULT";
        this->addDiagnosticsMessage("ERROR", "READ", { {"READ", "Unmanaged Exception! driver is in FAULT"} }, &report);
        CNR_ERROR_THROTTLE(m_logger, 5.0, "[ "<<p.second->getUniqueId()<<" ] " << report.str() );
        m_status = cnr_hardware_interface::StatusHw::ERROR;
        break;
    }
  }
  CNR_RETURN_TRUE_THROTTLE_DEFAULT(this->m_logger);
}

bool CoeRobotHW::doWrite(const ros::Time& /*time*/, const ros::Duration& /*period*/)
{
  CNR_TRACE_START_THROTTLE_DEFAULT(this->m_logger);

  double prev_write_time_ = write_time_;  
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  write_time_ =(double)(ts.tv_sec) +((double) ts.tv_nsec)/1.e9;
  if((prev_write_time_>0) &&(write_time_ - prev_write_time_ > 0.010))
  {
    CNR_WARN_THROTTLE(m_logger, 5.0, "The plugin writing latency is larger than 10ms, is it correct?");
  }
  
  for(auto& p: m_devices_map)
  {
    switch(p.second->write())
    {
      case coe_master::CoeHwPlugin::NONE_ERROR:
        break;
      case coe_master::CoeHwPlugin::COM_ERROR:
        CNR_ERROR_THROTTLE(m_logger, 5.0, "[ "<<p.second->getUniqueId()<<" ] WRITE from shmem failed."
                                << "Error in communication. EHI! You have to add the error management :)");
        m_status = cnr_hardware_interface::StatusHw::ERROR;
        break;
      case coe_master::CoeHwPlugin::DEVICE_ERROR:
        CNR_ERROR_THROTTLE(m_logger, 5.0, "[ "<<p.second->getUniqueId()<<" ] WRITE The driver is in FAULT"
                                <<" (or better, not in OPERATION ENABLED). Try to force reset.");
        m_status = cnr_hardware_interface::StatusHw::ERROR;
        break;
      case coe_master::CoeHwPlugin::EXCEPTION_ERROR:
        CNR_ERROR_THROTTLE(m_logger, 5.0, "[ "<<p.second->getUniqueId()<<" ] WRITE - Unmanaged Exception!"
                                << " Driver is in FAULT");
        m_status = cnr_hardware_interface::StatusHw::ERROR;
        break;
    }
  }
  CNR_RETURN_TRUE_THROTTLE_DEFAULT(this->m_logger);
}

void CoeRobotHW::shutdown()
{
  if(!m_shutted_down)
  {
    for(auto & device_info : m_devices_types_map)
    {
      std::string name = device_info.first;
      std::string type = device_info.second;
      m_devices_map.at(name)->setSoftRT();
      ros::Duration(0.001).sleep();
//      ROS_INFO("Delete Instance '%s' from heap ", name.c_str());
//      m_devices_map.at(name).reset();
//      ROS_INFO("Unconnect '%s' from heap ", name.c_str());
//      m_joint_to_device_map.at(name).reset();
//      ROS_INFO("Unload plugin '%s' from heap ", type.c_str());
//      m_planner_plugin_loader.at(name)->unloadLibraryForClass(type);
    }
//    m_joint_to_device_map.clear();
//    m_devices_map.clear();
//    m_planner_plugin_loader.clear();
//    BasicRobotHW::shutdown();
  }

}


}

